package edu.towson.cosc435.weagly.todos.ui.taskStuff

import java.time.LocalDate

data class Task(
    val title: String, //the title of the task (example: "go to bed")
    val contents: String, //the contents of the task (example: tuck yourself in and get some water)
    val completed: Boolean, //marks wether or not it has been completed
    val date: LocalDate  //an object that stores the date the task is due

    ) {
}