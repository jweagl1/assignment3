package edu.towson.cosc435.weagly.todos

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview
import edu.towson.cosc435.weagly.todos.ui.taskStuff.Task
import edu.towson.cosc435.weagly.todos.ui.MainScreen
import edu.towson.cosc435.weagly.todos.ui.userInteraction.UserVal
import edu.towson.cosc435.weagly.todos.ui.tasklist.TaskListView
import edu.towson.cosc435.weagly.todos.ui.theme.todosTheme
import java.time.LocalDate


class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalComposeUiApi::class)
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Log.d("TAG", "onCreate")
            todosTheme {
                Surface(color = MaterialTheme.colors.background) {
                    MainScreen()
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val tasks = (0..10).map { i ->
        Task("Task $i", "Contents $i", i % 3 == 0, LocalDate.now().plusDays(5)) //LocalDate.now().plusDays(5)
    }
    todosTheme {
        Surface(color = MaterialTheme.colors.background) {
            val vM = UserVal()
            TaskListView(tasks, tasks[0], vM, false, 0.0f, {}, {}, {})
        }
    }
}