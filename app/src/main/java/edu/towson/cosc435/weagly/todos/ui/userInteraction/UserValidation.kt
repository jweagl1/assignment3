package edu.towson.cosc435.weagly.todos.ui.userInteraction

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue


@Composable
fun ConfirmDialog(
    title: String,
    text: String,
    userVal: UserValI,
) {
    val show by userVal.userValDisplay
    if(show) { AlertDialog(
            onDismissRequest = { userVal.dismissDialog() },

            title = {


                if(!userVal.waiting.value) {
                    Text(title)
                }

                else {
                    Text("Deleting...")
                }

            },

            text = { if(userVal.waiting.value) {
                    Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.Center
                    ) {
                        LinearProgressIndicator(progress = userVal.waitingTimer.value)
                    }
                }
            else {

                Text(text)
                }

            },
            confirmButton = { Button({
                    userVal.onConfirmDelete()
                },
                enabled = !userVal.waiting.value) {
                    Text("Delete") }
            }
        )
    }
}