package edu.towson.cosc435.weagly.todos.ui.taskStuff

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class TaskDisplayFull : ViewModel() {
    private val selListVal: MutableState<Task?>
    val selectedTask: State<Task?>

    private val tasklistVal: MutableState<List<Task>> = mutableStateOf(listOf())
    val tasks: State<List<Task>> = tasklistVal

    private val progressVal: MutableState<Float>
    val waitingProgress: State<Float>

    private val waitingVal: MutableState<Boolean>
    val waiting: State<Boolean>



    private val _repository: TaskFunctionInterface = TaskFunctions()

    init {
        viewModelScope.launch {
            tasklistVal.value = _repository.getTasks()
        }
        selListVal = mutableStateOf(null)
        selectedTask = selListVal
        waitingVal = mutableStateOf(false)
        waiting = waitingVal
        progressVal = mutableStateOf(0.0f)
        waitingProgress = progressVal
    }

     fun addTask(task: Task, ) {
        viewModelScope.launch {
            waitingVal.value = true
            delay(5000)
            _repository.addTask(task)
            tasklistVal.value = _repository.getTasks()
            waitingVal.value = false
        }
    }

    suspend fun _incrementProgress() {
        while(true) {
            delay(500)
            progressVal.value += (1.0f / 10.0f)
        }
    }

    suspend fun deleteTaskAsync(idx: Int) {
        waitingVal.value = true
        progressVal.value = 0.0f
        val progJob = viewModelScope.async { _incrementProgress() }
        val deleteJob = viewModelScope.async { _repository.deleteTask(idx) }
        progJob.start()
        deleteJob.await()
        progJob.cancel()
        tasklistVal.value = _repository.getTasks()
        waitingVal.value = false
        progressVal.value = 0.0f
    }

    fun selectTask(task: Task) {
        selListVal.value = task
    }

    fun toggleCompleted(idx: Int) {
        viewModelScope.launch {
            _repository.toggleCompleted(idx)
            tasklistVal.value = _repository.getTasks()
        }
    }


}