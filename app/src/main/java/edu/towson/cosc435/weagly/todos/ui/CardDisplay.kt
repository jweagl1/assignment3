package edu.towson.cosc435.weagly.todos.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun Display(selectedTask: String?, content: @Composable () -> Unit) {
    Row() {
        Card(
            modifier = Modifier.size(200.dp).align(Alignment.CenterVertically)) {


            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            )
            {
                if(selectedTask != null)
                {
                    Text(selectedTask)
                }
            }
        }


        content()
    }
}