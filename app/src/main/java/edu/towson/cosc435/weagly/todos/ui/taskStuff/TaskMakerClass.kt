package edu.towson.cosc435.weagly.todos.ui.newtask

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import edu.towson.cosc435.weagly.todos.ui.taskStuff.Task
import java.time.LocalDate

class TaskMakerClass : ViewModel() {

    protected val contentVal: MutableState<String> = mutableStateOf("")
    val contents: State<String> = contentVal
    protected val taskVal: MutableState<String> = mutableStateOf("")
    val taskTitle: State<String> = taskVal
    protected val completionBoolean: MutableState<Boolean> = mutableStateOf(false)
    val completed: State<Boolean> = completionBoolean

    fun setTitle(title: String) {
        taskVal.value = title
    }

    fun setCompleted(isCompleted: Boolean) {
        completionBoolean.value = isCompleted
    }

    fun setContents(contents: String) {
        contentVal.value = contents
    }



    @RequiresApi(Build.VERSION_CODES.O)
    fun validate(): Task {
        if(taskTitle.value.isEmpty()) {
            throw Exception("Task does not have a title!")
        }
        if(contents.value.isEmpty()) {
            throw Exception("No contents!")
        }
        return Task(taskTitle.value, contents.value, completed.value, LocalDate.now().plusDays(5))
    }
}