package edu.towson.cosc435.weagly.todos.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.weagly.todos.ui.userInteraction.TaskAdder
import edu.towson.cosc435.weagly.todos.ui.userInteraction.TasksNavGraph

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@Composable
fun MainScreen() {
    val navigation = rememberNavController()
    Scaffold(
        topBar = {
            TopBar(nav = navigation)
        },
        bottomBar = {
            BottomBar(nav = navigation)
        }
    ) {
        TasksNavGraph(navigation) //api level 26?
    }
}

@Composable
private fun TopBar(
    nav: NavHostController
) {
    TopAppBar() {
        Text(text = "=")  //attempting to creathe the drawer navigation caused the app to not work
        Text(text = "        Tasks")
    }
}

@Composable
private fun BottomBar(
    nav: NavHostController 
) {
    BottomNavigation(
        elevation = 18.dp
    ) {
        Text("             ") //to create space so that the button is where it should be
        FloatingActionButton(onClick = {nav.navigate(TaskAdder.AddTask.route)}, //this button works! Sends user to other page
            modifier = Modifier
                .background(color = Color.Blue)
            ) {
            Icon(Icons.Filled.Add, contentDescription = null, tint = Color.White) //add icon. What is the hamburger icon? Where is it?
        }

    }
}