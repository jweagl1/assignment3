package edu.towson.cosc435.weagly.todos.ui.newtask

import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import edu.towson.cosc435.weagly.todos.ui.taskStuff.Task
import edu.towson.cosc435.weagly.todos.ui.theme.todosTheme
import java.lang.Exception

@ExperimentalComposeUiApi
@Composable
fun NewTaskView(
    vm: TaskMakerClass = viewModel(),
    onAddTask: (Task) -> Unit
) {
    val ctx = LocalContext.current
    val (titleTf, contentsTf, agendaTf) = remember { FocusRequester.createRefs() }
    LocalSoftwareKeyboardController.current
    LaunchedEffect(true) {
        titleTf.requestFocus()
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            "New Task",
            fontSize = 36.sp,
            modifier = Modifier.padding(16.dp).focusRequester(titleTf)
        )
        Text("Title", modifier = Modifier.padding(horizontal = 30.dp)) //this does nothing for some reason
        OutlinedTextField(
            value = vm.taskTitle.value,  onValueChange = vm::setTitle,
            singleLine = true,  modifier = Modifier
                .padding(16.dp)
                .focusRequester(titleTf),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Text

            ),
            keyboardActions = KeyboardActions(
                onNext = { contentsTf.requestFocus() }))

        Text("Contents", modifier = Modifier.padding(horizontal = 30.dp)) //this does nothing for some reason
        OutlinedTextField(

            value = vm.contents.value,
            onValueChange = vm::setContents,
            singleLine = true,
            modifier = Modifier
                .padding(16.dp)
                .focusRequester(contentsTf),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Text
            ),
            keyboardActions = KeyboardActions(
                onNext = { agendaTf.requestFocus() })
        )

        Row(
            modifier = Modifier.padding(16.dp)
        ) {

            Checkbox(
                checked = vm.completed.value,
                onCheckedChange = vm::setCompleted
            )

            Text(
                "Completed",
                modifier = Modifier.padding(end = 6.dp))

        }
        Button(

            onClick = {

                try {
                    val theTask = vm.validate()
                    onAddTask(theTask)
                } catch(e: Exception) {
                    Toast.makeText(ctx, e.toString(), Toast.LENGTH_SHORT).show() //not sure why this exception works but app crashes without it...
                }
            }
        ) {

            Text("Save") }
    }
}

@ExperimentalComposeUiApi
@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    todosTheme {
        val taskMake = TaskMakerClass()
        NewTaskView(taskMake, {})
    }
}
