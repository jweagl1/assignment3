package edu.towson.cosc435.weagly.todos.ui.userInteraction

import androidx.compose.runtime.State

interface UserValI {
    val userValDisplay: State<Boolean>
    val waiting: State<Boolean>
    val waitingTimer: State<Float>
    fun showConfirmDelete(onConfirm: suspend () -> Unit)
    fun onConfirmDelete()
    fun dismissDialog()
}