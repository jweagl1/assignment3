package edu.towson.cosc435.weagly.todos.ui.taskStuff

interface TaskFunctionInterface {
    suspend fun getTasks(): List<Task>
    suspend fun deleteTask(idx: Int)
    suspend fun toggleCompleted(idx: Int)
    suspend fun addTask(task: Task)
}