package edu.towson.cosc435.weagly.todos.ui

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.towson.cosc435.weagly.todos.ui.taskStuff.Task

@ExperimentalFoundationApi
@Composable
fun TaskDisplay(
    idx: Int,
    task: Task,
    onDelete: (Int) -> Unit,
    onToggle: (Int) -> Unit,
    onSelectTask: (Task) -> Unit
) {
    Log.d("TAG", task.title)
    Card(
        shape = RoundedCornerShape(5.dp),
        elevation = 16.dp,
        modifier = Modifier.padding(start=16.dp, end=16.dp, top=5.dp, bottom=5.dp).fillMaxWidth()
    )
    { Row(
            modifier = Modifier
                .combinedClickable(
                    onLongClick = {
                        onDelete(idx)
                    }
                ) {
                    onSelectTask(task)
                }.padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Column(


                modifier = Modifier.weight(1.5f)
            ) {
                Row(
                    modifier = Modifier.padding(5.dp),  verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(task.title, modifier = Modifier.weight(2.0f), fontSize = 28.sp, color = MaterialTheme.colors.secondary)
                }
                Row(
                    modifier = Modifier.padding(5.dp),  verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(task.contents, modifier = Modifier.weight(2.0f))
                }
                Row(
                    modifier = Modifier.padding(5.dp),  verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(task.date.toString(), modifier = Modifier.weight(2.0f)) //this displays the date
                }
            }
            Column(
                modifier = Modifier.weight(1.0f),  horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Spacer(modifier = Modifier.padding(bottom=5.dp))
                Row() {
                    Checkbox(checked = task.completed, onCheckedChange = { onToggle(idx) }, modifier = Modifier.padding(end=5.dp))
                }
            }
        }
    }
}
