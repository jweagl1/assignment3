package edu.towson.cosc435.weagly.todos.ui.userInteraction

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class UserVal : ViewModel(), UserValI {
    private val userValidation: MutableState<Boolean> = mutableStateOf(false)
    private var userDeletionEvent: (suspend () -> Unit)? = null
    override val userValDisplay: State<Boolean> = userValidation
    private val waitingval: MutableState<Boolean> = mutableStateOf(false)
    override val waiting: MutableState<Boolean> = waitingval
    private val waitingTime: MutableState<Float> = mutableStateOf(0.0f)
    override val waitingTimer: MutableState<Float> = waitingTime

    override fun onConfirmDelete() {
        if(userDeletionEvent != null) { viewModelScope.launch {
                userDeletionEvent?.invoke()
                dismissDialog() }
        }
    }

    override fun showConfirmDelete(onConfirm: suspend () -> Unit)
    {
        userValidation.value = true
        userDeletionEvent = onConfirm
    }
    override fun dismissDialog() { userValidation.value = false }
}