package edu.towson.cosc435.weagly.todos.ui.userInteraction

sealed class TaskAdder(val route: String) {
    object TaskList: TaskAdder("taskList")
    object AddTask: TaskAdder("addTask")
}