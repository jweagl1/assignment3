package edu.towson.cosc435.weagly.todos.ui.userInteraction

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.towson.cosc435.weagly.todos.ui.newtask.NewTaskView
import edu.towson.cosc435.weagly.todos.ui.newtask.TaskMakerClass
import edu.towson.cosc435.weagly.todos.ui.tasklist.TaskListView
import edu.towson.cosc435.weagly.todos.ui.taskStuff.TaskDisplayFull

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalComposeUiApi::class)
@ExperimentalFoundationApi
@Composable
fun TasksNavGraph(
    navController: NavHostController = rememberNavController()
) {
    val viewmodel = viewModel<TaskDisplayFull>()
    NavHost(
        navController = navController,
        startDestination = TaskAdder.TaskList.route
    ) {
        composable (TaskAdder.TaskList.route){
            val userValidation = viewModel<UserVal>()
            val tasks by viewmodel.tasks
            val selectedTask by viewmodel.selectedTask
            TaskListView(
                tasks,
                selectedTask,
                userValidation,
                waiting=viewmodel.waiting.value,
                waitingProgress=viewmodel.waitingProgress.value,
                onDelete=viewmodel::deleteTaskAsync,
                onToggle=viewmodel::toggleCompleted,
                onSelectTask=viewmodel::selectTask
            )
        }

        composable (TaskAdder.AddTask.route){
            val freshTask = viewModel<TaskMakerClass>()

            NewTaskView(

                vm = freshTask,
                onAddTask = { task ->
                    viewmodel.addTask(task)
                    navController.navigate(TaskAdder.TaskList.route)

                }
            )
        }
    }
}