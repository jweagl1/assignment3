package edu.towson.cosc435.weagly.todos.ui.taskStuff

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
class TaskFunctions : TaskFunctionInterface {

    private var taskListVar = listOf<Task>()

    init {
        taskListVar = (0..10).map { i ->
            Task("Task $i", "Contents $i", i % 3 == 0, LocalDate.now().plusDays(5)) //this adds 5 days to the date
        }
    }

    override suspend fun getTasks(): List<Task> {
        return taskListVar
    }

    override suspend fun deleteTask(idx: Int) {
        taskListVar = taskListVar.subList(0, idx) + taskListVar.subList(idx+1, taskListVar.size)
    }


    override suspend fun addTask(task: Task) {
        taskListVar = listOf(task) + taskListVar
    }

    override suspend fun toggleCompleted(idx: Int) {
        val existingTask = taskListVar.get(idx)
        val freshTask = existingTask.copy(completed = !existingTask.completed)
        taskListVar = taskListVar.subList(0, idx) + listOf(freshTask) + taskListVar.subList(idx+1, taskListVar.size)
    }
}