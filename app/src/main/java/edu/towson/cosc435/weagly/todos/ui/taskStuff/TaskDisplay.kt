package edu.towson.cosc435.weagly.todos.ui.tasklist

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalConfiguration
import edu.towson.cosc435.weagly.todos.ui.taskStuff.Task
import edu.towson.cosc435.weagly.todos.ui.userInteraction.UserVal
import edu.towson.cosc435.weagly.todos.ui.Display
import edu.towson.cosc435.weagly.todos.ui.TaskDisplay
import edu.towson.cosc435.weagly.todos.ui.userInteraction.ConfirmDialog

@ExperimentalFoundationApi
@Composable
fun TaskListView(
    
    tasks: List<Task>,
    selectedTask: Task?,
    confirmViewModel: UserVal,
    waiting: Boolean,
    waitingProgress: Float,
    onDelete: suspend (Int) -> Unit,
    onToggle: (Int) -> Unit,
    onSelectTask: (Task) -> Unit

) 
{
    confirmViewModel.waiting.value = waiting
    confirmViewModel.waitingTimer.value = waitingProgress
    Box(contentAlignment = Alignment.Center,) {
        ConfirmDialog(title = "Confirm",
            text = "Are you sure you want to delete?",
            userVal = confirmViewModel,)
        Column(modifier = Modifier
                .alpha(if(waiting) 0.2f else 1.0f)) {
            val conval = LocalConfiguration.current //val for configuration
            if (conval.orientation == Configuration.ORIENTATION_PORTRAIT) {
                LazyColumn {
                    itemsIndexed(tasks) { idx, task ->
                        TaskDisplay(idx, task, { idx ->
                            confirmViewModel.showConfirmDelete(onConfirm={ onDelete(idx) }) },
                            onToggle, onSelectTask) } }
            }

            else
            {
                Display(selectedTask?.title) {
                    LazyColumn {
                        itemsIndexed(tasks) { idx, task ->
                            TaskDisplay(idx, task, { idx ->
                                confirmViewModel.showConfirmDelete(onConfirm={ onDelete(idx) })
                            }, onToggle, onSelectTask)
                        }

                    }

                }

            }

        }


        if(waiting) { CircularProgressIndicator() }
    }
}
