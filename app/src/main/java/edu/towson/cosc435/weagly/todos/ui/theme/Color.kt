package edu.towson.cosc435.weagly.todos.ui.theme

import androidx.compose.ui.graphics.Color

val Pink = Color(0xFFE21195) //should be pink

val DarkRed = Color(0xFFB80A0A) //should be a dark red

val Black = Color(0xFF000000) //should be black
